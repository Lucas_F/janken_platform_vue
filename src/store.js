/* eslint-disable no-unused-vars */
import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import * as Cookies from "js-cookie";

Vue.use(Vuex);

const store = new Vuex.Store({
   state: {
      user: (Cookies.get('user') !== undefined ? JSON.parse(Cookies.get('user')) : '')
   },
   actions: {
      async getRooms({ commit }, body) {
         let result;

         if (body) {
            result = await axios.get(`http://localhost:8000/api/getRooms/${body}`)
         } else {
            result = await axios.get('http://localhost:8000/api/getRooms')
         }

         return result.data
      },
      async createRoom({ commit }, body) {
         await axios.post('http://localhost:8000/api/createRoom', body)
         return 'Room created'
      },
      async register({ commit }, body) {
         await axios.post('http://localhost:8000/api/register', body)
         return 'User registered'
      },
      async login({ commit, state }, body) {
         const result = await axios.post('http://localhost:8000/api/login', body)
         state.user = result.data
         Cookies.set('user', result.data)
         return 'User connected'
      },
      async logout({ commit, state }, body) {
         state.user = null
         Cookies.remove('user')
      },
      async getUsers({ commit }, body) {
         let result;

         if (body) {
            result = await axios.get(`http://localhost:8000/api/getUsers/${body}`)
         } else {
            result = await axios.get('http://localhost:8000/api/getUsers')
         }

         return result.data
      }
   }
})

export default store