import Vue from 'vue';
import VueRouter from "vue-router";
import Home from '@/pages/Home'
import Login from '@/pages/Login'
import Register from '@/pages/Register'
import Room from '@/pages/Room'

Vue.use(VueRouter);

const routes = [
   { path: '/', component: Home },
   { path: '/login', component: Login },
   { path: '/register', component: Register },
   { path: '/room/:id', component: Room }
]

const router = new VueRouter({
   mode: 'history',
   routes
})

export default router